module Main (main) where

import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Interact
import GHC.Float
import Data.List.Index
import Control.Monad
import qualified Data.Ord as Do

data State = Cross | Kreis deriving (Eq, Show)
data Board = Board State String [(Maybe State)] deriving (Eq, Show)
data GameEnd = GEX | GEO | GEDraw deriving (Eq, Show)

main :: IO ()
main = play (InWindow "Tic Tac Toe" windowSize (500, 500)) black 1 (Board Cross "" $ replicate 9 Nothing) draw io tick

draw :: Board -> Picture
draw (Board _ _ l) = Pictures $ grid : do
        x <- [(-1)..1]
        y <- [(-1)..1]
        let symbol = fmap drawp l !! coordToIndex (x, y)
        return $ Translate (int2Float x * size) (int2Float y * size) symbol

drawp :: Maybe State -> Picture
drawp (Just Cross) = cross
drawp (Just Kreis) = kreis
drawp _ = Blank

tick :: Float -> Board -> Board
tick _ w = w

io :: Event -> Board -> Board
io (EventKey (SpecialKey KeyEnter) Down _ _) _ = Board Cross "" $ replicate 9 Nothing
io (EventKey (MouseButton LeftButton) Down _ (x, y)) (Board turn _ oldBoard) =
    let xd = Do.clamp (-1, 1) $ round $ 3 * x / (int2Float $ fst windowSize) in
    let yd = Do.clamp (-1, 1) $ round $ 3 * y / (int2Float $ snd windowSize) in
    let idx = coordToIndex (xd, yd) in
    let newBoard = if oldBoard !! idx /= Nothing
        then Board turn "" oldBoard
        else Board (switch turn) (show idx) $ modifyAt idx (\_ -> Just turn) oldBoard in
    let st = gameEnd newBoard in
    if st /= Nothing
        then if st == (Just GEX) then
            Board Cross "" $ replicate 9 (Just Cross)
        else
            Board Cross "" $ replicate 9 (Just Kreis)
        else newBoard
io _ y = y

coordToIndex :: Num a => (a, a) -> a
coordToIndex (x, y) = 3 * (1 - y) + x + 1

switch :: State -> State
switch Cross = Kreis
switch Kreis = Cross

stateToEnd :: Maybe State -> Maybe GameEnd
stateToEnd (Just Cross) = Just GEX
stateToEnd (Just Kreis) = Just GEO
stateToEnd _ = Nothing

headMaybe :: [a] -> Maybe a
headMaybe [] = Nothing
headMaybe (x:_) = Just x

isSameC :: Eq a => [Maybe a] -> (Bool, Maybe a)
isSameC [] = (False, Nothing)
isSameC [a] = (a /= Nothing, a)
isSameC [a, b] = (a == b && a /= Nothing, a)
isSameC (x:xs) = ((fst $ isSameC [x, head xs]) && (fst $ isSameC xs), x)

windowSize :: (Int, Int)
windowSize = (600, 600)

size :: Float
size = (int2Float $ fst windowSize) / 3

sizeWithPadding :: Float
sizeWithPadding = (int2Float $ fst windowSize) * (3/11)

thickness :: Float
thickness = 5

grid :: Picture
grid =
    let gridLine = rectangleSolid (int2Float $ fst windowSize) thickness in
    Color grey $ Pictures $ do
        offset <- [(-size) / 2, size / 2]
        direction <- [0, 90]
        let g = Translate 0 offset gridLine
        return $ Rotate direction g

cross :: Picture
cross = Color red $ Pictures $ fmap ($ rectangleSolid (sizeWithPadding * (sqrt 2)) thickness) [Rotate 45, Rotate (-45)]

kreis :: Picture
kreis = Color blue $ ThickCircle (sizeWithPadding * 0.5) thickness

grey :: Color
grey = (makeColorI 120 120 120 255)

gameEnd :: Board -> Maybe GameEnd
gameEnd (Board _ _ b) =
    let r0 = isSameC [b !! 0, b !! 1, b !! 2] in
    let r1 = isSameC [b !! 3, b !! 4, b !! 5] in
    let r2 = isSameC [b !! 6, b !! 7, b !! 8] in
    let c0 = isSameC [b !! 0, b !! 3, b !! 6] in
    let c1 = isSameC [b !! 1, b !! 4, b !! 7] in
    let c2 = isSameC [b !! 2, b !! 5, b !! 8] in
    let d0 = isSameC [b !! 0, b !! 4, b !! 8] in
    let d1 = isSameC [b !! 2, b !! 4, b !! 6] in
    let list = [r0, r1, r2, c0, c1, c2, d0, d1] in
    stateToEnd $ join $ headMaybe $ fmap snd $ filter fst list